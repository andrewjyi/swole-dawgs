from fastapi import (
    Depends,
    Response,
    APIRouter,
)
from queries.exercises import ExercisesQueries
from models.accounts import AccountOut
from typing import Union, List
from models.exercises import ExerciseOut, TargetMuscle
from authenticator import authenticator
from models.errors import HttpError


router = APIRouter()


@router.get(
    "/api/exercises/{id}", response_model=Union[ExerciseOut, HttpError]
)
async def get_exercise(
    id: int,
    response: Response,
    exercises: ExercisesQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[ExerciseOut, HttpError]:
    if account:
        exercise = exercises.get_one(id=id)
        if type(exercise) is HttpError:
            response.status_code = 404
            return exercise
        return exercise
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.get(
    "/api/exercises", response_model=Union[List[ExerciseOut], HttpError]
)
async def list_exercises(
    response: Response,
    exercises: ExercisesQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[List[ExerciseOut], HttpError]:
    if account:
        exercises_list = exercises.get()
        if type(exercises_list) is HttpError:
            response.status_code = 404
            return exercises_list
        return exercises_list
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.post(
    "/api/exercises", response_model=Union[List[ExerciseOut], HttpError]
)
async def create_exercises(
    info: TargetMuscle,
    response: Response,
    exercises: ExercisesQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
):
    if account:
        exercises_list = exercises.create(info)
        if type(exercises_list) is HttpError:
            response.status_code = 404
            return exercises_list
        return exercises_list
    response.status_code = 401
    return HttpError(detail="User not logged in.")
