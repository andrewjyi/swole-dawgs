from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.workout_slice import WorkoutSliceQueries


client = TestClient(app=app)


def fake_get_current_account_data():
    return {"fake_data": "FAKE_DATA"}


class FakeWorkoutSliceQueries:
    def get(self):
        return []


def test_list_workout_slices():
    app.dependency_overrides[WorkoutSliceQueries] = FakeWorkoutSliceQueries
    app.dependency_overrides[authenticator.try_get_current_account_data] = (
        fake_get_current_account_data
    )
    res = client.get("/api/workout_slice")
    assert res.status_code == 200
    assert res.json() == []
