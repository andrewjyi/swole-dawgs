from fastapi.testclient import TestClient
from queries.exercise_slice import ExerciseSliceQueries
from authenticator import authenticator
from main import app

client = TestClient(app=app)

def fake_get_current_account_data():
    return {}

class FakeExerciseSliceQueries:
    def get(self):
        return []

def test_list_exercise_slices():
    # Arrange
    app.dependency_overrides[ExerciseSliceQueries] = FakeExerciseSliceQueries
    app.dependency_overrides[authenticator.try_get_current_account_data] = fake_get_current_account_data
    # Act
    res = client.get("/api/exercise_slice")


    # Assert
    assert res.status_code == 401
    assert res.json() == {"detail": "User not logged in"}
