from fastapi.testclient import TestClient
from main import app
from queries.workout_slice import WorkoutSliceQueries
from authenticator import authenticator
from models.workout_slice import WorkoutSliceIn, WorkoutSliceOut


client = TestClient(app=app)


def fake_try_get_current_account_data():
    return {
        "id": 1
    }

class WorkoutSliceQueriesMock:
    def get_one(self, id):
        workout_slice_data = {
            "workout_slice_id": id,
            "workout": {
                "name": "test2",
                "exercises": [
                {
                    "id": 1,
                    "exercise": {
                    "name": "3/4 sit-up",
                    "body_group": "push",
                    "body_part": "waist",
                    "equipment": "body weight",
                    "pic_url": "https://v2.exercisedb.io/image/UvNUmkBR0TsdqU",
                    "target": {
                        "name": "abs"
                    },
                    "descriptions": "Lie flat on your back with your knees bent and feet flat on the ground. Place your hands behind your head with your elbows pointing outwards. Engaging your abs, slowly lift your upper body off the ground, curling forward until your torso is at a 45-degree angle. Pause for a moment at the top, then slowly lower your upper body back down to the starting position. Repeat for the desired number of repetitions. ",
                    "id": 1
                    },
                    "sets": 3,
                    "reps": 10,
                    "is_complete": False,
                    "completed_on": None
                },
                {
                    "id": 2,
                    "exercise": {
                    "name": "45° side bend",
                    "body_group": "push",
                    "body_part": "waist",
                    "equipment": "body weight",
                    "pic_url": "https://v2.exercisedb.io/image/xzhlUPMFRcnWl4",
                    "target": {
                        "name": "abs"
                    },
                    "descriptions": "Stand with your feet shoulder-width apart and your arms extended straight down by your sides. Keeping your back straight and your core engaged, slowly bend your torso to one side, lowering your hand towards your knee. Pause for a moment at the bottom, then slowly return to the starting position. Repeat on the other side. Continue alternating sides for the desired number of repetitions. ",
                    "id": 2
                    },
                    "sets": 3,
                    "reps": 10,
                    "is_complete": False,
                    "completed_on": None
                },
                {
                    "id": 3,
                    "exercise": {
                    "name": "alternate lateral pulldown",
                    "body_group": "pull",
                    "body_part": "back",
                    "equipment": "cable",
                    "pic_url": "https://v2.exercisedb.io/image/Uv0Z9PWffiKK3U",
                    "target": {
                        "name": "lats"
                    },
                    "descriptions": "Sit on the cable machine with your back straight and feet flat on the ground. Grasp the handles with an overhand grip, slightly wider than shoulder-width apart. Lean back slightly and pull the handles towards your chest, squeezing your shoulder blades together. Pause for a moment at the peak of the movement, then slowly release the handles back to the starting position. Repeat for the desired number of repetitions. ",
                    "id": 7
                    },
                    "sets": 3,
                    "reps": 10,
                    "is_complete": False,
                    "completed_on": None
                },
                {
                    "id": 4,
                    "exercise": {
                    "name": "all fours squad stretch",
                    "body_group": "legs",
                    "body_part": "upper legs",
                    "equipment": "body weight",
                    "pic_url": "https://v2.exercisedb.io/image/yALrAta6avNLJJ",
                    "target": {
                        "name": "quads"
                    },
                    "descriptions": "Start on all fours with your hands directly under your shoulders and your knees directly under your hips. Extend one leg straight back, keeping your knee bent and your foot flexed. Slowly lower your hips towards the ground, feeling a stretch in your quads. Hold this position for 20-30 seconds. Switch legs and repeat the stretch on the other side. ",
                    "id": 1512
                    },
                    "sets": 3,
                    "reps": 10,
                    "is_complete": False,
                    "completed_on": None
                }
                ],
                "id": 11,
                "user_id": 1,
                "ppl": "push"
            },
            "date_complete": "2024-03-21T19:09:19.395722",
            "scheduled": None,
            "notes": "donezo",
            "is_complete": True,
            "user_id": 1
        }
        return WorkoutSliceOut(**workout_slice_data)

    def create(self, workout_slice, account_id):
        workout_slice_out_data = {
            "workout_slice_id": 3,
            "workout": {
                "name": "Backageddon",
                "exercises": [
                {
                    "id": 55,
                    "exercise": {
                    "name": "barbell shrug",
                    "body_group": "pull",
                    "body_part": "back",
                    "equipment": "barbell",
                    "pic_url": "https://v2.exercisedb.io/image/csHWIpIgfAuzUR",
                    "target": {
                        "name": "traps"
                    },
                    "descriptions": "Stand with your feet shoulder-width apart and hold a barbell in front of you with an overhand grip. Keep your arms straight and your back straight throughout the exercise. Lift your shoulders up towards your ears as high as possible, squeezing your traps at the top. Hold for a moment, then slowly lower your shoulders back down to the starting position. Repeat for the desired number of repetitions. ",
                    "id": 95
                    },
                    "sets": 3,
                    "reps": 10,
                    "is_complete": False,
                    "completed_on": None
                },
                {
                    "id": 56,
                    "exercise": {
                    "name": "barbell biceps curl (with arm blaster)",
                    "body_group": "pull",
                    "body_part": "upper arms",
                    "equipment": "barbell",
                    "pic_url": "https://v2.exercisedb.io/image/I3T4HZMf96Hn0g",
                    "target": {
                        "name": "biceps"
                    },
                    "descriptions": "Stand up straight with your feet shoulder-width apart and hold a barbell with an underhand grip, palms facing up. Place your upper arms against the arm blaster, keeping your elbows close to your torso. Keeping your upper arms stationary, exhale and curl the weights while contracting your biceps. Continue to raise the barbell until your biceps are fully contracted and the bar is at shoulder level. Hold the contracted position for a brief pause as you squeeze your biceps. Inhale and slowly begin to lower the barbell back to the starting position. Repeat for the desired number of repetitions. ",
                    "id": 2407
                    },
                    "sets": 4,
                    "reps": 10,
                    "is_complete": False,
                    "completed_on": None
                },
                {
                    "id": 57,
                    "exercise": {
                    "name": "barbell bent over row",
                    "body_group": "pull",
                    "body_part": "back",
                    "equipment": "barbell",
                    "pic_url": "https://v2.exercisedb.io/image/B4KUYg51qtbiik",
                    "target": {
                        "name": "upper back"
                    },
                    "descriptions": "Stand with your feet shoulder-width apart and knees slightly bent. Bend forward at the hips while keeping your back straight and chest up. Grasp the barbell with an overhand grip, hands slightly wider than shoulder-width apart. Pull the barbell towards your lower chest by retracting your shoulder blades and squeezing your back muscles. Pause for a moment at the top, then slowly lower the barbell back to the starting position. Repeat for the desired number of repetitions. ",
                    "id": 27
                    },
                    "sets": 3,
                    "reps": 10,
                    "is_complete": False,
                    "completed_on": None
                },
                {
                    "id": 58,
                    "exercise": {
                    "name": "barbell bent arm pullover",
                    "body_group": "pull",
                    "body_part": "back",
                    "equipment": "barbell",
                    "pic_url": "https://v2.exercisedb.io/image/CYOPUlQRXp8kv1",
                    "target": {
                        "name": "lats"
                    },
                    "descriptions": "Lie flat on a bench with your head at one end and your feet on the floor. Hold a barbell with a shoulder-width grip and extend your arms straight above your chest. Lower the barbell behind your head while keeping your arms slightly bent. Pause for a moment, then raise the barbell back to the starting position. Repeat for the desired number of repetitions. ",
                    "id": 1316
                    },
                    "sets": 3,
                    "reps": 15,
                    "is_complete": False,
                    "completed_on": None
                },
                {
                    "id": 59,
                    "exercise": {
                    "name": "alternate lateral pulldown",
                    "body_group": "pull",
                    "body_part": "back",
                    "equipment": "cable",
                    "pic_url": "https://v2.exercisedb.io/image/Uv0Z9PWffiKK3U",
                    "target": {
                        "name": "lats"
                    },
                    "descriptions": "Sit on the cable machine with your back straight and feet flat on the ground. Grasp the handles with an overhand grip, slightly wider than shoulder-width apart. Lean back slightly and pull the handles towards your chest, squeezing your shoulder blades together. Pause for a moment at the peak of the movement, then slowly release the handles back to the starting position. Repeat for the desired number of repetitions. ",
                    "id": 7
                    },
                    "sets": 4,
                    "reps": 12,
                    "is_complete": False,
                    "completed_on": None
                }
                ],
                "id": workout_slice.workout_id,
                "user_id": None,
                "ppl": "pull"
            },
            "date_complete": "2024-03-21T22:02:11.273058",
            "scheduled": workout_slice.scheduled,
            "notes": workout_slice.notes,
            "is_complete": workout_slice.is_complete,
            "user_id": account_id
        }
        return WorkoutSliceOut(**workout_slice_out_data)

def test_get_workout_slice():

    id=1
    app.dependency_overrides[WorkoutSliceQueries] = WorkoutSliceQueriesMock

    result = client.get(f"/api/workout_slice/{id}")

    assert result.status_code == 200
    assert result.json() == {
        "workout_slice_id": 1,
        "workout": {
            "name": "test2",
            "exercises": [
            {
                "id": 1,
                "exercise": {
                "name": "3/4 sit-up",
                "body_group": "push",
                "body_part": "waist",
                "equipment": "body weight",
                "pic_url": "https://v2.exercisedb.io/image/UvNUmkBR0TsdqU",
                "target": {
                    "name": "abs"
                },
                "descriptions": "Lie flat on your back with your knees bent and feet flat on the ground. Place your hands behind your head with your elbows pointing outwards. Engaging your abs, slowly lift your upper body off the ground, curling forward until your torso is at a 45-degree angle. Pause for a moment at the top, then slowly lower your upper body back down to the starting position. Repeat for the desired number of repetitions. ",
                "id": 1
                },
                "sets": 3,
                "reps": 10,
                "is_complete": False,
                "completed_on": None
            },
            {
                "id": 2,
                "exercise": {
                "name": "45° side bend",
                "body_group": "push",
                "body_part": "waist",
                "equipment": "body weight",
                "pic_url": "https://v2.exercisedb.io/image/xzhlUPMFRcnWl4",
                "target": {
                    "name": "abs"
                },
                "descriptions": "Stand with your feet shoulder-width apart and your arms extended straight down by your sides. Keeping your back straight and your core engaged, slowly bend your torso to one side, lowering your hand towards your knee. Pause for a moment at the bottom, then slowly return to the starting position. Repeat on the other side. Continue alternating sides for the desired number of repetitions. ",
                "id": 2
                },
                "sets": 3,
                "reps": 10,
                "is_complete": False,
                "completed_on": None
            },
            {
                "id": 3,
                "exercise": {
                "name": "alternate lateral pulldown",
                "body_group": "pull",
                "body_part": "back",
                "equipment": "cable",
                "pic_url": "https://v2.exercisedb.io/image/Uv0Z9PWffiKK3U",
                "target": {
                    "name": "lats"
                },
                "descriptions": "Sit on the cable machine with your back straight and feet flat on the ground. Grasp the handles with an overhand grip, slightly wider than shoulder-width apart. Lean back slightly and pull the handles towards your chest, squeezing your shoulder blades together. Pause for a moment at the peak of the movement, then slowly release the handles back to the starting position. Repeat for the desired number of repetitions. ",
                "id": 7
                },
                "sets": 3,
                "reps": 10,
                "is_complete": False,
                "completed_on": None
            },
            {
                "id": 4,
                "exercise": {
                "name": "all fours squad stretch",
                "body_group": "legs",
                "body_part": "upper legs",
                "equipment": "body weight",
                "pic_url": "https://v2.exercisedb.io/image/yALrAta6avNLJJ",
                "target": {
                    "name": "quads"
                },
                "descriptions": "Start on all fours with your hands directly under your shoulders and your knees directly under your hips. Extend one leg straight back, keeping your knee bent and your foot flexed. Slowly lower your hips towards the ground, feeling a stretch in your quads. Hold this position for 20-30 seconds. Switch legs and repeat the stretch on the other side. ",
                "id": 1512
                },
                "sets": 3,
                "reps": 10,
                "is_complete": False,
                "completed_on": None
            }
            ],
            "id": 11,
            "user_id": 1,
            "ppl": "push"
        },
        "date_complete": "2024-03-21T19:09:19.395722",
        "scheduled": None,
        "notes": "donezo",
        "is_complete": True,
        "user_id": 1
        }

def test_create_workout_slice():

    body = {
        "workout_id": 2,
        "scheduled": "2024-03-21T22:01:53.372Z",
        "notes": "string",
        "is_complete": True
    }

    app.dependency_overrides[WorkoutSliceQueries] = WorkoutSliceQueriesMock
    app.dependency_overrides[authenticator.try_get_current_account_data] = fake_try_get_current_account_data

    result = client.post("/api/workout_slice", json=body)

    assert result.status_code == 200
    assert result.json() == {
        "workout_slice_id": 3,
        "workout": {
            "name": "Backageddon",
            "exercises": [
            {
                "id": 55,
                "exercise": {
                "name": "barbell shrug",
                "body_group": "pull",
                "body_part": "back",
                "equipment": "barbell",
                "pic_url": "https://v2.exercisedb.io/image/csHWIpIgfAuzUR",
                "target": {
                    "name": "traps"
                },
                "descriptions": "Stand with your feet shoulder-width apart and hold a barbell in front of you with an overhand grip. Keep your arms straight and your back straight throughout the exercise. Lift your shoulders up towards your ears as high as possible, squeezing your traps at the top. Hold for a moment, then slowly lower your shoulders back down to the starting position. Repeat for the desired number of repetitions. ",
                "id": 95
                },
                "sets": 3,
                "reps": 10,
                "is_complete": False,
                "completed_on": None
            },
            {
                "id": 56,
                "exercise": {
                "name": "barbell biceps curl (with arm blaster)",
                "body_group": "pull",
                "body_part": "upper arms",
                "equipment": "barbell",
                "pic_url": "https://v2.exercisedb.io/image/I3T4HZMf96Hn0g",
                "target": {
                    "name": "biceps"
                },
                "descriptions": "Stand up straight with your feet shoulder-width apart and hold a barbell with an underhand grip, palms facing up. Place your upper arms against the arm blaster, keeping your elbows close to your torso. Keeping your upper arms stationary, exhale and curl the weights while contracting your biceps. Continue to raise the barbell until your biceps are fully contracted and the bar is at shoulder level. Hold the contracted position for a brief pause as you squeeze your biceps. Inhale and slowly begin to lower the barbell back to the starting position. Repeat for the desired number of repetitions. ",
                "id": 2407
                },
                "sets": 4,
                "reps": 10,
                "is_complete": False,
                "completed_on": None
            },
            {
                "id": 57,
                "exercise": {
                "name": "barbell bent over row",
                "body_group": "pull",
                "body_part": "back",
                "equipment": "barbell",
                "pic_url": "https://v2.exercisedb.io/image/B4KUYg51qtbiik",
                "target": {
                    "name": "upper back"
                },
                "descriptions": "Stand with your feet shoulder-width apart and knees slightly bent. Bend forward at the hips while keeping your back straight and chest up. Grasp the barbell with an overhand grip, hands slightly wider than shoulder-width apart. Pull the barbell towards your lower chest by retracting your shoulder blades and squeezing your back muscles. Pause for a moment at the top, then slowly lower the barbell back to the starting position. Repeat for the desired number of repetitions. ",
                "id": 27
                },
                "sets": 3,
                "reps": 10,
                "is_complete": False,
                "completed_on": None
            },
            {
                "id": 58,
                "exercise": {
                "name": "barbell bent arm pullover",
                "body_group": "pull",
                "body_part": "back",
                "equipment": "barbell",
                "pic_url": "https://v2.exercisedb.io/image/CYOPUlQRXp8kv1",
                "target": {
                    "name": "lats"
                },
                "descriptions": "Lie flat on a bench with your head at one end and your feet on the floor. Hold a barbell with a shoulder-width grip and extend your arms straight above your chest. Lower the barbell behind your head while keeping your arms slightly bent. Pause for a moment, then raise the barbell back to the starting position. Repeat for the desired number of repetitions. ",
                "id": 1316
                },
                "sets": 3,
                "reps": 15,
                "is_complete": False,
                "completed_on": None
            },
            {
                "id": 59,
                "exercise": {
                "name": "alternate lateral pulldown",
                "body_group": "pull",
                "body_part": "back",
                "equipment": "cable",
                "pic_url": "https://v2.exercisedb.io/image/Uv0Z9PWffiKK3U",
                "target": {
                    "name": "lats"
                },
                "descriptions": "Sit on the cable machine with your back straight and feet flat on the ground. Grasp the handles with an overhand grip, slightly wider than shoulder-width apart. Lean back slightly and pull the handles towards your chest, squeezing your shoulder blades together. Pause for a moment at the peak of the movement, then slowly release the handles back to the starting position. Repeat for the desired number of repetitions. ",
                "id": 7
                },
                "sets": 4,
                "reps": 12,
                "is_complete": False,
                "completed_on": None
            }
            ],
            "id": 2,
            "user_id": None,
            "ppl": "pull"
        },
        "date_complete": "2024-03-21T22:02:11.273058",
        "scheduled": "2024-03-21T22:01:53.372000+00:00",
        "notes": "string",
        "is_complete": True,
        "user_id": 1
    }
