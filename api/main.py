from fastapi import FastAPI
from authenticator import authenticator
from routers import (
    accounts,
    exercises,
    exercise_slice,
    workouts,
    workout_slice,
)
from fastapi.middleware.cors import CORSMiddleware
import os


app = FastAPI()
app.include_router(authenticator.router, tags=["Authenticator"])
app.include_router(accounts.router, tags=["Accounts"])
app.include_router(exercises.router, tags=["Exercises"])
app.include_router(exercise_slice.router, tags=["Exercise Slice"])
app.include_router(workouts.router, tags=["Workouts"])
app.include_router(workout_slice.router, tags=["Workout Slice"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
