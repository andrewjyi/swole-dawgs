steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE workout (
            
            id SERIAL PRIMARY KEY NOT NULL, 
            name VARCHAR(50),
            user_id INTEGER REFERENCES account(id),
            ppl VARCHAR(5)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE workout;
        """,
    ],
]
