def push_pull_legs(target):
    body_group_dict = {
        "push": [
            "abs",
            "delts",
            "forearms",
            "levator scapulae",
            "pectorals",
            "serratus anterior",
            "triceps",
        ],
        "pull": ["biceps", "lats", "spine", "traps", "upper back"],
        "legs": [
            "calves",
            "quads",
            "hamstrings",
            "glutes",
            "abductors",
            "adductors",
        ],
    }
    for key, value in body_group_dict.items():
        if target in value:
            return key
    return None
