import { Link } from 'react-router-dom'
import { useGetLastWorkoutQuery, useGetWorkoutListQuery, useGetUserTokenQuery } from '../apiSlice'
import { useState, useEffect } from 'react'

const RecommendedWorkout = () => {
    const { data: workouts, isLoading: workoutsLoading } =
        useGetWorkoutListQuery()
    const { data: account, isLoading: accountLoading } = useGetUserTokenQuery();
    const { data: lastWorkout, isLoading } = useGetLastWorkoutQuery()
    const [lastWorkoutType, setLastWorkoutType] = useState()
    const [recWorkout, setRecWorkout] = useState()

    const chooseWorkout = () => {
        let recommendType
        if (lastWorkoutType) {
            if (lastWorkoutType === 'push') {
                recommendType = 'legs'
            } else if (lastWorkoutType === 'legs') {
                recommendType = 'pull'
            } else {
                recommendType = 'push'
            }
        } else {
            recommendType = 'push'
        }
        const workoutsFiltered = workouts.filter(
                (workout) => workout.ppl === recommendType && (workout.user_id === Number(account.account.id) || workout.user_id === null )
            )
            return workoutsFiltered[0]
    }

    useEffect(() => {
        if (workouts && account) {
            setRecWorkout(chooseWorkout())
        }
    }, [isLoading, workoutsLoading, lastWorkoutType, lastWorkout, workouts, account, accountLoading])

    useEffect(() => {
        if (!isLoading && lastWorkout?.workout) {
            setLastWorkoutType(lastWorkout.workout.ppl)
        }
    }, [isLoading, lastWorkout])

    if (isLoading || workoutsLoading) return <div>Loading...</div>

    return (
        <div className="container my-3">
            <div className="card">
                <div className="card-header">Recommended Workout</div>
                <div className="card-body">
                    {recWorkout && (
                        <>
                            <h5 className="card-title">{recWorkout.name}</h5>
                            {recWorkout.body_group}
                            <p className="card-text"></p>
                            <table className='table'>
                                <thead className='table-dark'>
                                    <tr>
                                        <th>Name</th>
                                        <th>Sets</th>
                                        <th>Reps</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {recWorkout.exercises.map((exercise) => {
                                        return (
                                            <tr key={exercise.id}>
                                                <td>{exercise.exercise.name}</td>
                                                <td>{exercise.sets}</td>
                                                <td>{exercise.reps}</td>
                                            </tr>
                                        )
                                    }
                                    )}
                                </tbody>
                            </table>
                            <Link to={`/workouts/${recWorkout.id}`}>
                                <button className="btn btn-primary">
                                    WORKOUT!
                                </button>
                            </Link>
                        </>
                    )}
                </div>
            </div>
        </div>
    )
}

export default RecommendedWorkout
