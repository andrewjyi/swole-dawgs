import { configureStore } from '@reduxjs/toolkit'
import { swoleApi } from './apiSlice'

export const store = configureStore({
    reducer: {
        [swoleApi.reducerPath]: swoleApi.reducer
        
    },

    middleware: (getDefaultMiddleware) => 
        getDefaultMiddleware().concat(swoleApi.middleware)
})